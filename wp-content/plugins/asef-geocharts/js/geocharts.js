function drawCharts(){
  
  google.charts.load('current', {
    'packages':['geochart'],
    'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
  });
  google.charts.setOnLoadCallback(drawRegionsMap);

}

function drawRegionsMap() {

  var rawData;
  var ajaxData = {
    action: 'geochart_data',
    type: 'json'
  }

  jQuery.getJSON(js_object.ajax_url, ajaxData, function(response) {
      
      rawData = Object.values(response);
      
      var data = google.visualization.arrayToDataTable(rawData);
      var options = {
        'dataMode' : 'regions',
        'colorAxis': {colors: [js_object.bg_color, js_object.bg_color]},
        'datalessRegionColor': js_object.default_color,
        'defaultColor': js_object.jadefault_color,
        'legend': 'none',
      };

      var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

      function mapEventHandler(){
        var selectedItem = chart.getSelection()[0];
        if (selectedItem) {
          var country = data.getValue(selectedItem.row, 0);
          window.open(window.location.origin+"/events/category/"+country, "_blank");  
        }
        
      }

      google.visualization.events.addListener(chart, 'select', mapEventHandler);

      chart.draw(data, options);

      
  });
  
}

drawCharts();

// jQuery(window).resize(function(){
//   drawCharts();  
// });




