// /////////////////////////////////////////
// Required Dependencies
// /////////////////////////////////////////
var gulp 			= require('gulp'),
	sass 			= require('gulp-sass');
	
var config = {
	css_filename: 'geocharts.css',
	css_destination: 'css/'
}

// /////////////////////////////////////////
// Sass/Less Tasks
// /////////////////////////////////////////
gulp.task('sass', function(){

	var path = require('path');

  	return gulp.src('geocharts/app.scss')
    .pipe(sass({
      paths: [ path.join(__dirname, 'scss', 'includes') ]
    }))
    .pipe(gulp.dest(config.css_destination));

});


// /////////////////////////////////////////
// Watch Tasks
// /////////////////////////////////////////
gulp.task('watch', function(){
	// gulp.watch(config.js_concat_files, ['scripts']);
	gulp.watch(['cc/geocharts.scss', 'css/*','!'+config.css_destination+config.css_filename], ['sass']);
	// gulp.watch(config.css_concat_files, ['concat']);
	// gulp.watch('wp-content/themes/sound-planning/**/*.html', ['html']);
	// gulp.watch('*.php', ['php']);
});
// /////////////////////////////////////////
// Default Tasks
// /////////////////////////////////////////
// gulp.task('default', ['sass', 'scripts', 'html', 'php', 'browser-sync', 'watch']);
gulp.task('default', ['sass', 'watch']);
