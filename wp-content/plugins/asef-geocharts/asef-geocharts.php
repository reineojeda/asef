<?php
/**
 * Plugin Name: Asef Geo Charts
 * Description: Custom Plugin For Asef Geo Charts
 * Version: 1.0
 * Author: First Rain Solutions
 * Author URI: http://firstrainsolutions.com
 */



function asef_geocharts($atts) {

 	$plugin_url = plugin_dir_url( __FILE__ );

 	 wp_enqueue_style( 'geocharts-css', $plugin_url.'css/geocharts.css', 'parent-stylesheet', '1.0'); 

 	$useragent = $_SERVER['HTTP_USER_AGENT'];

	if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){

		$tableContent = getchart_data();

		$content = '<div class="geocharts-container"><table class="table geocharts">';
		
		foreach ($tableContent as $key => $value) {
			
			$content .= '<tr>';
			foreach ($value as $val) {
				$content .= '<td>';
				$content .= $val;
				$content .= '</td>';
				
			}
			$content .= '<tr>';
		}

		$content .= '</table></div>';

	}else{

		$options = get_option('geocharts_options_group');
	 	$bgColor = $options['geocharts_background_color'];
	 	$defaultColor = $options['geocharts_default_color'];



		wp_register_script( 'geocharts-script', $plugin_url.'js/geocharts.js', array( 'jquery' ), '1.0', true );

		wp_enqueue_script( 'geocharts-script');

	    wp_localize_script( 'geocharts-script', 'js_object', array( 
	    	'ajax_url' => admin_url( 'admin-ajax.php'),
	    	'bg_color' => $bgColor,
	    	'default_color' => $defaultColor
	    ));
	   


	 	$content = '<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>';
	 	$content .='<div id="regions_div" style="width: 900px; height: 500px;"></div>';	
	}

    return $content;
}

	
add_shortcode('geocharts', 'asef_geocharts');



function wp_ajax_geochart_data() {
    
	$data = getchart_data();
    echo json_encode($data);
    wp_die();
}

function getchart_data(){

	global $wpdb;

    //get plugin settings
   	$options = get_option('geocharts_options_group');
    $in = $options['geocharts_inbound_key'];
    $out = $options['geocharts_outbound_key'];
    $legends = $options['geocharts_legend'];
    $legendsArr = explode(",", $legends);

	$query  = "SELECT meta_value,
			sum( case meta_key when '".$in."' then 1 else 0 end ) as 'in', 
    		sum( case meta_key when '".$out."' then 1 else 0 end ) as 'out'
  			from {$wpdb->prefix}usermeta
 			where meta_key IN ('".$in."', '".$out."')
 			group by meta_value";

	$results = $wpdb->get_results($query, ARRAY_A);
	$data[] = $legendsArr;

	foreach ($results as $dataKey => $dataValue) {

		$rawArray = array_values($dataValue);

		//convert from string to int

		foreach ($rawArray as $arraKey => $arrayValue) {
			$in = $rawArray[1];
			$rawArray[1] = (int) $in;

			$out = $rawArray[2];
			$rawArray[2] = (int) $out;
		}

		$data[] = $rawArray;
	}

	return $data;
}

add_action( 'wp_ajax_nopriv_geochart_data', 'wp_ajax_geochart_data' );
add_action( 'wp_ajax_geochart_data', 'wp_ajax_geochart_data' );

/** CREATE SETTINGS LINK **/
add_filter('plugin_action_links_'.plugin_basename(__FILE__), 'geocharts_add_plugin_page_settings_link');
function geocharts_add_plugin_page_settings_link( $links ) {
	$links[] = '<a href="' .
		admin_url( 'options-general.php?page=asef-geocharts' ) .
		'">' . __('Settings') . '</a>';
	return $links;
}

add_action( 'admin_init', 'geocharts_page_init' );

function geocharts_page_init()
    {        
        register_setting( 'geocharts_options', 'geocharts_options_group');
        
	    add_settings_section(
	        'geocharts_plugin_section',
	        __(''),
	        'geocharts_settings_section_callback',
	        'geocharts_options'
	    );

	    add_settings_field(
	        'geocharts_inbound_key',
	        __( 'Inbound Meta Key', 'wordpress' ),
	        'geocharts_inbound_key_render',
	        'geocharts_options',
	        'geocharts_plugin_section'
	    ); 

	    add_settings_field(
	        'geocharts_outbound_key',
	        __( 'Outbound Meta Key', 'wordpress' ),
	        'geocharts_outbound_key_render',
	        'geocharts_options',
	        'geocharts_plugin_section'
	    ); 

	    add_settings_field(
	        'geocharts_lender',
	        __( 'Legends', 'wordpress' ),
	        'geocharts_legend_render',
	        'geocharts_options',
	        'geocharts_plugin_section'
	    );

	     
		add_settings_field(
	        'geocharts_background_color',
	        __( 'Background Color', 'wordpress' ),
	        'geocharts_background_color_render',
	        'geocharts_options',
	        'geocharts_plugin_section'
	    ); 

	    add_settings_field(
	        'geocharts_default_color',
	        __( 'Default Color', 'wordpress' ),
	        'geocharts_default_color_render',
	        'geocharts_options',
	        'geocharts_plugin_section'
	    );  
    }



/** CREATE CONFIG SETTINGS **/

function geocharts_register_options_page() {
  add_options_page('Geocharts Settings', 'Asef GeoCharts', 'manage_options', 'asef-geocharts', 'geocharts_options_page');
}

add_action('admin_menu', 'geocharts_register_options_page');

/** FORM RENDER **/


function geocharts_inbound_key_render() {
    $options = get_option( 'geocharts_options_group' );
    ?>
    <input type='text' name='geocharts_options_group[geocharts_inbound_key]' value='<?php echo $options['geocharts_inbound_key']; ?>'>
    <?php
}

function geocharts_outbound_key_render() {
    $options = get_option( 'geocharts_options_group' );
    ?>
    <input type='text' name='geocharts_options_group[geocharts_outbound_key]' value="<?php echo $options['geocharts_outbound_key']; ?>">
    <?php
}

function geocharts_legend_render() {
    $options = get_option( 'geocharts_options_group' );
    ?>
     <textarea id="geocharts_legend" name="geocharts_options_group[geocharts_legend]" value="<?php echo $options['geocharts_legend']; ?>"><?php echo $options['geocharts_legend']; ?></textarea>
    <?php
}

function geocharts_background_color_render() {
    $options = get_option('geocharts_options_group');
    ?>
    <input type='text' name='geocharts_options_group[geocharts_background_color]' value="<?php echo $options['geocharts_background_color']; ?>">
    <?php
}

function geocharts_default_color_render() {
    $options = get_option('geocharts_options_group');
    ?>
    <input type='text' name='geocharts_options_group[geocharts_default_color]' value="<?php echo $options['geocharts_default_color']; ?>">
    <?php
}

function geocharts_settings_section_callback(  ) {
    echo __( 'Modify the geocharts referrence', 'wordpress' );
}

function geocharts_options_page()
{ ?>
  	<div class="well">
  		<?php screen_icon(); ?>
  		<h1>ASEF GeoCharts Configuration</h1> 	
  		<form method="post" action="options.php">
  			<?php do_settings_sections( 'geocharts_options' ); ?>
  			<?php  settings_fields( 'geocharts_options' ); ?>
  			
  			<?php  submit_button(); ?>
  		</form>
  	</div>

<?php }